
PhpStorm
--------
* Import code style XML file from ./phpstorm.xml to PhpStorm
* Open import window
```
File -> Settings -> Code Style -> PHP -> Import Scheme -> Intellij IDEA code style XML
```

PhpStorm Inspections
--------
* Import inspections profile XML file from ./phpstorm-inspections.xml to PhpStorm
* Open import window
```
File -> Settings -> Inspections -> Import Profile
```
